from gamelib import *
import random
import sys

class ChainReactionGame(Game):
    def __init__(self):
        super().__init__(1000, 800)
        self.background = Rect(self, 0, self.width/2, self.height/2, self.width, self.height, (255, 255, 255))
        self.terminal = Terminal(self, 2, self.width/4, self.height/2, self.width/2, self.height, (0, 0, 0))
        self.rect = Rect(self, 1, self.width/2 + 150, self.height/2 - 100, 400, 700, (0, 0, 255))
        self.move_right = False
        self.wow = False
        self.fall = False
        self.hit = False
        self.text = False
        self.time = 0

    def update(self, dt):
        print(self.time)
        if self.time == 0:
            self._entities.add(Text(game, 2, "make_ball(10, 10);", (0, 255, 0, 255), 10, game.height - 50))
            # self.time += 1/

        if round(self.time) == 3 and self.wow == False:
            self.play_sound("assets/wow.mp3")
            RedBall(self, 50, self.height / 2 + 200, 672, 50)
            self.wow = True
            # Text(game, 2, "ball_bigger()", (0, 255, 0, 255), 10, game.height - 40)

        if round(self.time) == 4 and self.move_right == False:
            self._entities.add(Text(game, 2, "move_right();", (0, 255, 0, 255), 10, game.height - 80))
            self.move_right = True
            # ball = self.get_entities_of_type(RedBall)[0]
            # ball.scale += 1 * dt


        if self.time > 5 and self.time < 7.7 and self.move_right == True:
            ball = self.get_entities_of_type(RedBall)[0]
            ball.rotation += 4
            ball.x += 100 * dt

        if self.time > 7.7 and self.fall == False:
            self.play_sound("assets/fall.mp3")
            self.fall = True
        
        
        if self.time > 10 and self.text == False:
            self.play_sound("assets/ohnooo.mp3")
            self._entities.add(Text(game, 2, "OHNOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO", (0, 255, 0, 255), 10, game.height - 110))
            self.text = True
        
        if self.time > 7.7 and self.time < 14.2:
            # self.play_sound("assets/fall.mp3")
            # self.play_sound("assets/wow.mp3")
            ball = self.get_entities_of_type(RedBall)[0]
            ball.rotation += 1
            ball.y -= 100 * dt
        
        # if self.time > 14.22 :
        #     pass

        if self.time > 13 and self.hit == False:
            self.play_sound("assets/explo.mp3")
            self.hit = True

        if self.time > 14.2:
            text = self.get_entities_of_type(Text)[0]
            text1 = self.get_entities_of_type(Text)[1]
            text2 = self.get_entities_of_type(Text)[2]

            text.y -= 100 * dt
            text1.y -= 100 * dt
            text2.y -= 100 * dt
            
            text.x += random.randint(-100, 100) * dt
            text1.x += random.randint(-100, 100) * dt
            text2.x += random.randint(-100, 100) * dt

            text.opacity -= 1
            text1.opacity -= 1
            text2.opacity -= 1
            if text.opacity == 0:
                sys.exit()

        self.time += dt


        #Text(self, 2, "Hello world", (0, 255, 0, 255), 10, self.height - 20)

    # def run(self):
    #     super().run()
    #     # while True:


class Text(LabelEntity):
    def __init__(self, game, layer, text, color, x, y):
        super().__init__(game, "", layer)
        self.x = x + self.width/2
        self.y = y
        self.color = color
        self.x_text = text
        self.letter_index = 0
        self.extra_letter = 0.1

    def update(self, dt):
        if self.letter_index >= len(self.x_text):
            return
        super().update(dt)
        self.extra_letter -= dt
        if self.extra_letter <= 0:
            self.text = self.text + "" + self.x_text[self.letter_index]
            self.x = 10 + self.width/2
            self.letter_index += 1
            self.extra_letter = 0.1

    

class Rect(RectEntity):
    def __init__(self, game, layer, x, y, width, height, color):
        super().__init__(game, layer)
        self.width = width
        self.height = height
        self.x = x 
        self.y = y
        self.color = color

class RedBall(SpriteEntity):
    def __init__(self, game: Game, radius: int, x: int, y: int, layer: int = 0):
        image_file = "assets/Red-Ball-Transparent.png"  # Path to the image file representing a red ball
        super().__init__(game, image_file, layer=layer, x=x, y=y)
        self.width = radius * 2
        self.height = radius * 2
        self.scale = radius / 100.0  # Adjust scale based on the desired size of the ball
    
    # def update(self, dt):
    #     self.x += 20 * dt
    #     self.y += 20 * dt



class Terminal:
    def __init__(self, game, layer, x, y, width, height, color):
        self.x = x
        self.y = y
        self.windowBody = Rect(game, layer, self.x, self.y, width, height, color)
        self.windowTitleBar = Rect(game, layer+1, self.x, self.y-18+height/2, width, 36, (24,153,213))
        self.titleBarMinBtn = Rect(game, layer+2, self.x+width/2-70-18, self.y-18+height/2, 25, 25, (56,104,144))
        self.titleBarMaxBtn = Rect(game, layer+2, self.x+width/2-35-18, self.y-18+height/2, 25, 25, (56,104,144))
        self.titleBarCloseBtn = Rect(game, layer+2, self.x+width/2-18, self.y-18+height/2, 25, 25, (255,70,70))

game = ChainReactionGame()
game.run()
