from __future__ import annotations
import pyglet
from abc import ABC, abstractmethod
from gamelib import *


class RedBall(SpriteEntity):
    def __init__(self, game: Game, radius: int, x: int, y: int, layer: int = 0):
        image_file = "red_ball.png"  # Path to the image file representing a red ball
        super().__init__(game, image_file, layer=layer, x=x, y=y)
        self.width = radius * 2
        self.height = radius * 2
        self.scale = radius / 100.0  # Adjust scale based on the desired size of the ball
